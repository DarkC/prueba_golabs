import { Component,OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	mediaSource = new MediaSource();
    sourceBuffer;
    public recordedBlobs = [];
    public mediaRecorder;
    recordButton = document.querySelector('button#record');
	async init() {
		var constraints = { audio: true, video: { width: 1280, height: 720 }};
	  	try {
	    const stream = await navigator.mediaDevices.getUserMedia(constraints);
	    this.handleSuccess(stream);
	  	} catch (e) {
	    console.error('navigator.getUserMedia error:', e);	
	  	}
	  	this.recordedBlobs = [];
  	}
  	play(){
	  	 const superBuffer = new Blob(this.recordedBlobs, {type: 'video/webm'});
	  	document.querySelector('video#recorded').src = null;
	  	document.querySelector('video#recorded').srcObject = null;
	  	document.querySelector('video#recorded').src = window.URL.createObjectURL(superBuffer);
	  	document.querySelector('video#recorded').controls = true;
	  	document.querySelector('video#recorded').play();
  	}
   	handleSourceOpen(event) {
	  	console.log('MediaSource opened');
	  	this.sourceBuffer = this.mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
	  	console.log('Source buffer: ', this.sourceBuffer);
	}
  	ngOnInit() {
    	this.mediaSource.addEventListener('sourceopen', this.handleSourceOpen, false);
  	}
  	handleSuccess(stream) {
	  	console.log('getUserMedia() got stream:', stream);
	  	document.querySelector('button#record').disabled = false;
	  	window.stream = stream;
	   	const gumVideo = document.querySelector('video#gum').srcObject = stream;
	}
	startRecording() {
  	this.recordedBlobs = [];
  	let options = {mimeType: 'video/webm;codecs=vp9'};
  	if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.error(`${options.mimeType} is not Supported`);
    options = {mimeType: 'video/webm;codecs=vp8'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.error(`${options.mimeType} is not Supported`);
      options = {mimeType: 'video/webm'};
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.error(`${options.mimeType} is not Supported`);
        options = {mimeType: ''};
      }
    }
  	}
  	try {
    	this.mediaRecorder = new MediaRecorder(window.stream, options);
  	} catch (e) {
    	console.error('Exception while creating MediaRecorder:', e);
    	return;
  	}

	  console.log('Created MediaRecorder', this.mediaRecorder, 'with options', options);
	  document.querySelector('button#stop').disabled = false;
	  document.querySelector('button#download').disabled = true;
	  this.mediaRecorder.onstop = (event) => {
	    console.log('Recorder stopped: ', event);
	  };
	  this.mediaRecorder.ondataavailable = this.handleDataAvailable;
	  this.mediaRecorder.start(10); // collect 10ms of data
	  console.log('MediaRecorder started', this.mediaRecorder);
	}
	handleDataAvailable(event) {
  	if (event.data && event.data.size > 0) {
    	this.recordedBlobs.push(event.data);
  	}
	}
	stopRecording() {
  	this.mediaRecorder.stop();
  	console.log('Recorded Blobs: ', this.recordedBlobs);
 	document.querySelector('button#download').disabled = false;
	}
	download(){
		const blob = new Blob(this.recordedBlobs, {type: 'video/webm'});
	  	const url = window.URL.createObjectURL(blob);
	  	const a = document.createElement('a');
	  	a.style.display = 'none';
	  	a.href = url;
	  	a.download = 'test.webm';
	  	document.body.appendChild(a);
	  	a.click();
	  	setTimeout(() => {
	    document.body.removeChild(a);
	    window.URL.revokeObjectURL(url);
	  	}, 100);
	}
}